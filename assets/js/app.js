$(function () {
    'use strict';
    if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
        var msViewportStyle = document.createElement('style');
        msViewportStyle.appendChild(
            document.createTextNode(
                '@-ms-viewport{width:auto!important}'
            )
        );
        document.querySelector('head').appendChild(msViewportStyle);
    }

    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();

    Array.prototype.move = function (from, to) {
        this.splice(to, 0, this.splice(from, 1)[0]);
    };

    $('.btnWhyUpgrade').on('click',function(){
        $('body,html').animate({
            scrollTop: $(".container.highlights").offset().top - 60
        }, 750);
    });

    $('.toTop').on('click',function(){
        $('body,html').animate({
            scrollTop: 0
        }, 750);
    });
});
