<cfset strSampleJSON = '{
  "id": "cus_9ztkH7YNkSHkpv",
  "object": "customer",
  "account_balance": 0,
  "created": 1485362914,
  "currency": "usd",
  "default_source": "card_19ftwj2eZvKYlo2CEozLxdy0",
  "delinquent": false,
  "description": null,
  "discount": null,
  "email": "atvm16+pt1@gmail.com",
  "livemode": false,
  "metadata": {
  },
  "shipping": null,
  "sources": {
    "object": "list",
    "data": [
      {
        "id": "card_19ftwj2eZvKYlo2CEozLxdy0",
        "object": "card",
        "address_city": null,
        "address_country": null,
        "address_line1": null,
        "address_line1_check": null,
        "address_line2": null,
        "address_state": null,
        "address_zip": null,
        "address_zip_check": null,
        "brand": "Visa",
        "country": "US",
        "customer": "cus_9ztkH7YNkSHkpv",
        "cvc_check": "pass",
        "dynamic_last4": null,
        "exp_month": 2,
        "exp_year": 2022,
        "funding": "credit",
        "last4": "4242",
        "metadata": {
        },
        "name": null,
        "tokenization_method": null
      }
    ],
    "has_more": false,
    "total_count": 1,
    "url": "/v1/customers/cus_9ztkH7YNkSHkpv/sources"
  },
  "subscriptions": {
    "object": "list",
    "data": [

    ],
    "has_more": false,
    "total_count": 0,
    "url": "/v1/customers/cus_9ztkH7YNkSHkpv/subscriptions"
  }
}'>
<cfset stcSampledata = DeserializeJSON(strSampleJSON)>

<h3>Testing Array Append by Reference</h3>
<p>Pass Array By Reference Enabled:<cfoutput> #application.getApplicationSettings().getPassArrayByReference()#</cfoutput></p>
<p>Complex Object: <cfoutput>#strSampleJSON#</cfoutput></p>
<p>Appending Complex Object to Array <cfoutput>#application.testLoopLength#</cfoutput> times</p>

<cffunction name="appendArrayByReference" hint="Modifies the array that is passed in." returntype="void">
    <cfargument name="array" type="array">
    <cfargument name="value" type="any">
    <cfset ArrayAppend(arguments.array,value)>
</cffunction>

<cfset tickBegin = GetTickCount()>
<cfset arrTest = ArrayNew(1,false)>
<cfloop from="0" to="#application.testLoopLength#" index="index">
    <cfset stcSampledata.id = "cus_" & CreateUUID()>
    <cfset appendArrayByReference(arrTest,stcSampledata)>
</cfloop>
<cfset tickEnd = GetTickCount()>
<cfset testTime = tickEnd - tickBegin>

<!--- Report the results of the test --->
<cfoutput>Test time was: #testTime# milliseconds</cfoutput>
