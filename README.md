# README #

This project contains a micro-site for training/demonstration of ColdFusion 2016.

### Requirements ###

* ColdFusion 2016
* IIS w/ URL Rewrite Module, available [here](https://www.microsoft.com/en-us/download/details.aspx?id=47337)
* Assumes installation is at http://localhost/cf2016-training, otherwise you'll need to update application.baseUrl in application.cfc