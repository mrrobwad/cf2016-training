<div class="container container-main">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <h2>ColdFusion Aether</h2>
                <img src="<cfoutput>#application.baseUrl#/assets/images/aether.png</cfoutput>" class="img-responsive">
                <h4>What's next for ColdFusion?</h4>
                <ul>
                    <li>
                        Implement a modern platform (looking more like other Java frameworks?)
                        <ul>
                            <li>
                                Improved object oriented support
                                <ul>
                                    <li>Support of covariant return types - If a function returns "any" type, then more specific return type should be allowed</li>
                                    <li>Covariant for argument type - Like return, covariant support for arguments too</li>
                                    <li>Method overloading support</li>
                                    <li>Support abstract Component</li>
                                    <li>Static functions and variables</li>
                                    <li>Every literal, or expression is an object</li>
                                    <li>Additional member functions</li>
                                </ul>
                            </li>
                            <li>
                                CFScript 2.0!
                                <ul>
                                    <li><code>&lt;cfscriptversion=2&gt;</code></li>
                                    <li>Get rid of the generic syntax from tags</li>
                                    <li><code>cfdbinfo(type=&quot;tables&quot;,name=&quot;info&quot;); // old way</code></li>
                                    <li><code>info = dbinfo(&quot;tables&quot;); // new way</code></li>
                                </ul>
                            </li>
                            <li>
                                Support for NULL
                                <ul>
                                    <li>In CF, empty string "" is considered undefined</li>
                                    <li>Interoperability issues with other technologies</li>
                                    <li>A null from JavaScript or database is serialized to "" (empty string)</li>
                                    <li>Introduce the null keyword, allow variables to be assigned to null</li>
                                </ul>
                            </li>
                            <li>
                                Improved Multi-threading support
                                <ul>
                                    <li>Allow synchronization in CFML code at function and block levels
                                    <li>Provide a simpler way by introducing "synchronized" keyword
                                    <li>Execute a task asynchronously</li>
                                    <li>Introduce runAsync() function</li>
                                    <li>Support for Atomic datatypes : Atomic Integer/Boolean</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        Leverage containerization
                        <ul>
                            <li>
                                Provide official docker images for CF 2016 and CF next
                                <ul>
                                    <li>Continuous deployment and testing</li>
                                    <li>Identical dev test prod setup</li>
                                    <li>Reduced upgrade and reinstallation times</li>
                                    <li>Replicate across CF nodes</li>
                                    <li>Multi-cloud platforms – simplify portability</li>
                                    <li>Version control</li>
                                    <li>Isolation – apps running on its own stack within containers</li>
                                    <li>Security - segregation and isolation –hacks cannot propagate to other containers</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        Provision microservices architecture
                        <ul>
                            <li>
                                Services separation and caching
                                <ul>
                                    <li>Evaluate services separation - Smaller footprint for specific functionality</li>
                                    <li>Support for distributed cache</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        Embrace devsecops
                        <ul>
                            <li>Tool to lockdown</li>
                            <li>Built-in Web Application firewall</li>
                        </ul>
                    </li>
                    <li>
                        Accelerate digital transformation - API Management
                        <ul>
                            <li>
                                Strategy - Near Term
                                <ul>
                                    <li>Performance and Scalability - Maintain lead position</li>
                                    <li>Security - Threat protection, 2 way SSL</li>
                                    <li>API Design - Data transformation, Multitenancy</li>
                                    <li>Analytics - User engagement, API Auditing</li>
                                </ul>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                Strategy - Long Term
                                <ul>
                                    <li>SaaS and hybrid deployment models</li>
                                    <li>Cloud connectors and orchestration</li>
                                    <li>API Discovery and Marketplace</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <!--- Begin Footer --->
    <footer>
        <p class="pull-right"><a href="javascript:;" class="toTop">Back to top</a></p>
        <p>&copy; 2016 <a href="https://plus.google.com/u/0/+RobertWaddell?rel=author" target="_blank">Robert Waddell</a></p>
    </footer>
    <!--- End Footer --->
</div>
