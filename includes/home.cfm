<div class="carousel slide">
    <div class="carousel-inner" role="listbox">
        <cfoutput>
            <div class="item active" style="background-image: url(#application.baseUrl#/assets/images/homepage-short.jpg);background-size:cover;">
                <div class="container">
                    <div class="carousel-caption" style="background-color:rgba(45, 189, 149, 0.45);padding:20px;bottom:60px;">
                        <h1>ColdFusion 2016</h1>
                        <p>ColdFusion 2016 introduces new constructs and various performance improvements.</p>
                        <p>Where is ColdFusion headed? How can we leverage the enhancements in CF2016?</p>
                        <p><a class="btnWhyUpgrade btn btn-lg btn-primary" href="javascript:;" role="button">Why Upgrade?</a></p>
                    </div>
                </div>
            </div>
        </cfoutput>
    </div>
</div>

<div class="container marketing highlights">
    <div class="row">
        <div class="col-lg-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-bar-chart fa-stack-1x text-danger-bright"></i>
            </span>
            <h2>Performance</h2>
            <p>New settings and constructs are available that can significantly boost performance once implemented. Details are available under the Enhancements menu option above.</p>
            <p>Right out of the box, ColdFusion 2016 offers a performance improvement of 30% <i>(BlogCFC sample application on CF11 vs CF2016)</i>.</p>
        </div>
        <div class="col-lg-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-lock fa-stack-1x text-info-bright"></i>
            </span>
            <h2>Security</h2>
            <p>The new Security Analyzer detects security vulnerability in code, line by line and organized by threat type and severity.</p>
            <p>CFAdmin is only accessible via internal web server by default. CFScripts now has its own directory, no longer in CFIDE. Numerous underlying libraries have been updated.</p>
        </div>
        <div class="col-lg-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-code fa-stack-1x text-white"></i>
            </span>
            <h2>Language</h2>
            <p>Numerous improvements and additions have been made to language constructs such as new member functions, enhancements to existing functions/tags, and new functions/tags and constructs.</p>
            <p>Some highlights include a Safe Navigation operator, Struct Serialization enhancements, and ability to set encoding type on cfoutput tag.</p>
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-12">
            <h2 class="featurette-heading" style="margin-top:0;">CF Summit 2016</h2>
            <p class="lead">Check out a quick video of some highlights from CF Summit 2016, you may see some familiar faces!</p>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/QOPsMUuMCL4"></iframe>
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7 col-md-push-5">
            <h2 class="featurette-heading">Oh yeah, it's alive and kicking. <span class="text-muted">See for yourself.</span></h2>
            <p class="lead">Based upon the ColdFusion Usage Survey 2016 shared by Adobe, we can get some interesting insights into the health and usage of ColdFusion.</p>
        </div>
        <div class="col-md-5 col-md-pull-7">
            <div class="row">
                <div class="col-xs-3">
                    <span class="fa-stack fa-4x">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <span class="fa-stack-1x text-success-bright" style="font-size: .75em;">70%</span>
                    </span>
                </div>
                <div class="col-xs-9 overview-item">
                    <h2>build new applications with ColdFusion</h2>
               </div>
            </div>
            <div class="row">
                <div class="col-xs-3">
                    <span class="fa-stack fa-4x">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <span class="fa-stack-1x text-success-bright" style="font-size: .75em;">86%</span>
                    </span>
                </div>
                <div class="col-xs-9 overview-item">
                    <h2>external-facing ColdFusion applications</h2>
               </div>
            </div>
            <div class="row">
                <div class="col-xs-3">
                    <span class="fa-stack fa-4x">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <span class="fa-stack-1x text-success-bright" style="font-size: .75em;">71%</span>
                    </span>
                </div>
                <div class="col-xs-9 overview-item">
                    <h2>expose services as APIs</h2>
               </div>
            </div>
            <div class="row">
                <div class="col-xs-3">
                    <span class="fa-stack fa-4x">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <span class="fa-stack-1x text-success-bright" style="font-size: .75em;">30%</span>
                    </span>
                </div>
                <div class="col-xs-9 overview-item">
                    <h2>deploy ColdFusion on the cloud</h2> <i>(55% in AWS)</i>
               </div>
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-12">
            <h2 class="featurette-heading" style="margin-top:0;">20 years...</h2>
            <p class="lead">Listen to what some key ColdFusion personalities such as Rakshith Naresh, Charlie Arehart, Elishia Dvorak, Adam Lehman, Tridib Roy Chowdhury, and others have to say about ColdFusion.</p>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/OpYEGMqMovc"></iframe>
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7">
            <h2 class="featurette-heading">ColdFusion's Not Dead. <span class="text-muted">Check the numbers.</span></h2>
        </div>
        <div class="col-md-5">
            <div class="row">
                <div class="col-xs-3">
                    <span class="fa-stack fa-4x">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-chain fa-stack-1x text-success-bright"></i>
                    </span>
                </div>
                <div class="col-xs-9 overview-item">
                    <h2>ColdFusion is 21 years young.</h2>
               </div>
            </div>
            <div class="row">
                <div class="col-xs-3">
                    <span class="fa-stack fa-4x">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-bar-chart fa-stack-1x text-success-bright"></i>
                    </span>
                </div>
                <div class="col-xs-9 overview-item">
                    <h2>Double-digit growth launch-over-launch.</h2>
               </div>
            </div>
            <div class="row">
                <div class="col-xs-3">
                    <span class="fa-stack fa-4x">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-users fa-stack-1x text-success-bright"></i>
                    </span>
                </div>
                <div class="col-xs-9 overview-item">
                    <h2>2000 new ColdFusion customers were added in the last year.</h2> <i>(as of Oct 2016)</i>
               </div>
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <!--- Begin Footer --->
    <footer>
        <p class="pull-right"><a href="javascript:;" class="toTop">Back to top</a></p>
        <p>&copy; 2016 <a href="https://plus.google.com/u/0/+RobertWaddell?rel=author" target="_blank">Robert Waddell</a></p>
    </footer>
    <!--- End Footer --->
</div>
