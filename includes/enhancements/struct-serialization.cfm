<div class="container container-main">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <h2>Struct Serialization</h2>
                <p>Up through ColdFusion 11, struct serialization did automatic type conversion. There was no way to prevent this without writing cumbersome UDFs.</p>
                <p>In CF2016, you specifiy the type of of each key explicitly so that the type is maintained and not auto-converted.</p>
            </div>

            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#syntax" aria-controls="syntax" role="tab" data-toggle="tab">Syntax</a></li>
                    <li role="presentation"><a href="#cf11" aria-controls="cf11" role="tab" data-toggle="tab">CF11 Example</a></li>
                    <li role="presentation"><a href="#cf2016" aria-controls="cf2016" role="tab" data-toggle="tab">CF2016 Example</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="syntax">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>stcToSerialize.setMetaData({KEY_NAME:{type:"string"}});</code><br>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="cf11">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>arrToSerialize = ArrayNew(1);</code><br>
                                <code>stcToSerialize = {"PRODUCT_TITLE": "Headphones", "PRODUCT_PRICE": "999", "PRODUCT_BRAND": "YES"};</code><br>
                                <code>arrToSerialize.append(stcToSerialize);</code><br>
                                <code>WriteDump(SerializeJSON(arrToSerialize));</code><br><br>

                                <code>// output (note converted type for PRODUCT_BRAND)</code><br>
                                <cfscript>
                                arrToSerialize = ArrayNew(1);
                                stcToSerialize = {"PRODUCT_TITLE": "Headphones", "PRODUCT_PRICE": "999", "PRODUCT_BRAND": "YES"};
                                arrToSerialize.append(stcToSerialize);
                                WriteOutput('<code>');
                                WriteOutput(SerializeJSON(arrToSerialize));
                                WriteOutput('</code>');
                                </cfscript>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="cf2016">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>arrToSerialize = ArrayNew(1);</code><br>
                                <code>stcToSerialize = {"PRODUCT_TITLE": "Headphones", "PRODUCT_PRICE": "999", "PRODUCT_BRAND": "YES"};</code><br>
                                <code>stcToSerialize.setMetaData({PRODUCT_BRAND:{type:"string"}});</code><br>
                                <code>arrToSerialize.append(stcToSerialize);</code><br>
                                <code>WriteDump(SerializeJSON(arrToSerialize));</code><br><br>

                                <code>// output (note proper type for PRODUCT_BRAND)</code><br>
                                <cfscript>
                                arrToSerialize = ArrayNew(1);
                                stcToSerialize = {"PRODUCT_TITLE": "Headphones", "PRODUCT_PRICE": "999", "PRODUCT_BRAND": "YES"};
                                stcToSerialize.setMetaData({PRODUCT_BRAND:{type:"string"}});
                                arrToSerialize.append(stcToSerialize);
                                WriteOutput('<code>');
                                WriteOutput(SerializeJSON(arrToSerialize));
                                WriteOutput('</code>');
                                </cfscript>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <!--- Begin Footer --->
    <footer>
        <p class="pull-right"><a href="javascript:;" class="toTop">Back to top</a></p>
        <p>&copy; 2016 <a href="https://plus.google.com/u/0/+RobertWaddell?rel=author" target="_blank">Robert Waddell</a></p>
    </footer>
    <!--- End Footer --->
</div>
