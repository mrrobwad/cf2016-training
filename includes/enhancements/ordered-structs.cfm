<div class="container container-main">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <h2>Ordered Structs</h2>
                <p>Up through ColdFusion 11, structs had a random order. If you wanted to order a struct you had to convert it into an array and sort the array.</p>
                <p>In CF2016, you can directly sort the struct. Unlike sorting an array, the order of the struct must be defined when the struct is created by passing one or two arguments to <code>StructNew()</code>.</p>
            </div>

            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#syntax" aria-controls="syntax" role="tab" data-toggle="tab">Syntax</a></li>
                    <li role="presentation"><a href="#cf11" aria-controls="cf11" role="tab" data-toggle="tab">CF11 Example</a></li>
                    <li role="presentation"><a href="#cf2016" aria-controls="cf2016" role="tab" data-toggle="tab">CF2016 Example</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="syntax">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>myStruct = StructNew(structType, sortType, sortOrder, localeSensitive);</code><br><br>
                                <code>//You can also create an ordered struct via a sorting callback:</code><br>
                                <code>myStruct = StructNew(callback);</code><br><br>
                                <code>//You can also create an ordered struct using a literal syntax:</code><br>
                                <code>myStruct = [zach: "mastodon", jason: "tyrannosaurus"];</code><br>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="cf11">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>stcPowerRangers = StructNew();</code><br>
                                <code>stcPowerRangers.tommy = "dragon";</code><br>
                                <code>stcPowerRangers.jason = "tyrannosaurus";</code><br>
                                <code>stcPowerRangers.zach = "mastodon";</code><br>
                                <code>stcPowerRangers.kimberly = "pterodactyl";</code><br>
                                <code>stcPowerRangers.trini = "saber tooth tiger";</code><br>
                                <code>stcPowerRangers.billy = "triceratops";</code><br><br>

                                <code>// random output order like:</code><br>
                                <cfscript>
                                stcPowerRangers = StructNew();
                                stcPowerRangers.tommy = "dragon";
                                stcPowerRangers.jason = "tyrannosaurus";
                                stcPowerRangers.zach = "mastodon";
                                stcPowerRangers.kimberly = "pterodactyl";
                                stcPowerRangers.trini = "saber tooth tiger";
                                stcPowerRangers.billy = "triceratops";
                                for (key in stcPowerRangers) {
                                    WriteOutput('<code>');
                                    WriteOutput(key);
                                    WriteOutput('</code><br>');
                                }
                                </cfscript>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="cf2016">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>stcPowerRangers = StructNew("ordered");</code><br>
                                <code>stcPowerRangers.tommy = "dragon";</code><br>
                                <code>stcPowerRangers.jason = "tyrannosaurus";</code><br>
                                <code>stcPowerRangers.zach = "mastodon";</code><br>
                                <code>stcPowerRangers.kimberly = "pterodactyl";</code><br>
                                <code>stcPowerRangers.trini = "saber tooth tiger";</code><br>
                                <code>stcPowerRangers.billy = "triceratops";</code><br><br>
                                <code>// output</code><br>
                                <cfscript>
                                stcPowerRangers = StructNew("ordered");
                                stcPowerRangers.tommy = "dragon";
                                stcPowerRangers.jason = "tyrannosaurus";
                                stcPowerRangers.zach = "mastodon";
                                stcPowerRangers.kimberly = "pterodactyl";
                                stcPowerRangers.trini = "saber tooth tiger";
                                stcPowerRangers.billy = "triceratops";
                                for (key in stcPowerRangers) {
                                    WriteOutput('<code>');
                                    WriteOutput(key);
                                    WriteOutput('</code><br>');
                                }
                                </cfscript>
                                <br>
                                <code>stcPowerRangers = StructNew("ordered", "text", "asc");</code><br>
                                <code>stcPowerRangers.tommy = "dragon";</code><br>
                                <code>stcPowerRangers.jason = "tyrannosaurus";</code><br>
                                <code>stcPowerRangers.zach = "mastodon";</code><br>
                                <code>stcPowerRangers.kimberly = "pterodactyl";</code><br>
                                <code>stcPowerRangers.trini = "saber tooth tiger";</code><br>
                                <code>stcPowerRangers.billy = "triceratops";</code><br><br>
                                <code>// output</code><br>
                                <cfscript>
                                stcPowerRangers = StructNew("ordered", "text", "asc");
                                stcPowerRangers.tommy = "dragon";
                                stcPowerRangers.jason = "tyrannosaurus";
                                stcPowerRangers.zach = "mastodon";
                                stcPowerRangers.kimberly = "pterodactyl";
                                stcPowerRangers.trini = "saber tooth tiger";
                                stcPowerRangers.billy = "triceratops";
                                for (key in stcPowerRangers) {
                                    WriteOutput('<code>');
                                    WriteOutput(key);
                                    WriteOutput('</code><br>');
                                }
                                </cfscript>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <!--- Begin Footer --->
    <footer>
        <p class="pull-right"><a href="javascript:;" class="toTop">Back to top</a></p>
        <p>&copy; 2016 <a href="https://plus.google.com/u/0/+RobertWaddell?rel=author" target="_blank">Robert Waddell</a></p>
    </footer>
    <!--- End Footer --->
</div>
