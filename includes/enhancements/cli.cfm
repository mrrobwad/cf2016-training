<div class="container container-main">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <h2>Command Line Interface</h2>
                <p>CF2016 has a new CLI that supports execution of ColdFusion code from the command line without running any server. Arguments and named arguments can also be passed to the command line interface. The lookup for Application.cfc depends on the wwwroot, which is set according to the path of cfm (absolute or relative). In Application.cfc, only onApplicationStart(), onApplicationStop(), and onError() methods are supported. There is no support for session and request methods in CLI. The following scopes are supported in the CLI: application, argument, request, this</p>
                <h4>Run a cfm from command line (with or without named/unnamed arguments)</h4>
                <p>
                    <code>$ cf.bat myfile.cfm</code><br>
                    <code>$ cf.bat myfile.cfm 10 myArg=test</code>
                </p>
                <h4>Interface with the CLI via the CFM</h4>
                <p>
                    <code>cli.getArgs(); // Gets all the arguments.</code><br>
                    <code>cli.getNamedArgs(); // Gets all the named arguments.</code><br>
                    <code>cli.getUnnamedArgs(); // Returns all the unnamed args as a CFML array or an empty array if none are specified.</code><br>
                    <code>cli.getArg(int index); // Gets the argument at the index location.</code><br>
                    <code>cli.getNamedArg(String argName); // Gets the value of the named argument with name argName.</code><br>
                    <code>cli.read(); // Reads one line from stdin.</code><br>
                    <code>cli.writeln(message); // Writes the message string to stdout.</code><br>
                    <code>cli.writeError(errorMessage); // Writes the error message string to stderr.</code><br>
                    <code>cli.exit(exitCode); // The exit function takes an optional status code and exits to the console with the specified exit code. By default, CLI returns an exitcode of 0 on successful execution and 1 when execution fails.</code><br>
                </p>
                <h4>Notable Supported Features</h4>
                <p>Mail, File, Database, Web Services</p>
                <h4>Unsupported Features</h4>
                <p>Charting, Scheduled Tasks, PDF features, Document, REST, Solr, Flex Integration, DotNet Integration, WebSocket, Image Functions, API Manager</p>
                <h4>More Info</h4>
                <p><a href="http://helpx.adobe.com/coldfusion/2016/command-line-interface.html" target="_blank">http://helpx.adobe.com/coldfusion/2016/command-line-interface.html</a><br></p>
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <!--- Begin Footer --->
    <footer>
        <p class="pull-right"><a href="javascript:;" class="toTop">Back to top</a></p>
        <p>&copy; 2016 <a href="https://plus.google.com/u/0/+RobertWaddell?rel=author" target="_blank">Robert Waddell</a></p>
    </footer>
    <!--- End Footer --->
</div>
