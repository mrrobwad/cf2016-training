<div class="container container-main">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <h2>Safe Navigation</h2>
                <p>Up through ColdFusion 11, safety checks were performed via <code>IsDefined()</code>, <code>StructKeyExists()</code>, <code>IsNull()</code>, etc. This was cumbersome when dealing with lots of data, specifically with nested structs.</p>
                <p>In CF2016, you can now leverage the safe navigation operator (<code>?.</code>) to do quick and tidy safety checks. These can be strung together in chains for nested objects and can even be used to safety check function results. You can also use the safe navigation operator with the Elvis operator to get the default value in case of an error.</p>
                <p class="lead">This can be especially useful when consuming data that is generated with an inconsistent data structure.</p>
            </div>

            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#syntax" aria-controls="syntax" role="tab" data-toggle="tab">Syntax</a></li>
                    <li role="presentation"><a href="#cf11" aria-controls="cf11" role="tab" data-toggle="tab">CF11 Example</a></li>
                    <li role="presentation"><a href="#cf2016" aria-controls="cf2016" role="tab" data-toggle="tab">CF2016 Example</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="syntax">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>result = myvar?.firstlevel?.nextlevel?.udf()?.trim();</code><br>
                                <code>result = myvar?.firstlevel?.nextlevel?.udf()?.trim() ?: "There was an error.";</code><br>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="cf11">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>x = apiCallGetX();</code><br>
                                <code>if(isDefined("x.result")){</code><br>
                                <code>&nbsp;&nbsp;y = x.result.getY();</code><br>
                                <code>&nbsp;&nbsp;if(isDefined("y.result")){</code><br>
                                <code>&nbsp;&nbsp;&nbsp;&nbsp;z = y.result.getZ();</code><br>
                                <code>&nbsp;&nbsp;&nbsp;&nbsp;if(isDefined("z.result")){</code><br>
                                <code>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;writeOutput(z.result);</code><br>
                                <code>&nbsp;&nbsp;&nbsp;&nbsp;}</code><br>
                                <code>&nbsp;&nbsp;}</code><br>
                                <code>}</code>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="cf2016">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>x = apiCallGetX();</code><br>
                                <code>writeOutput(x?.result?.getY()?.result?.getZ()?.result);</code><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <!--- Begin Footer --->
    <footer>
        <p class="pull-right"><a href="javascript:;" class="toTop">Back to top</a></p>
        <p>&copy; 2016 <a href="https://plus.google.com/u/0/+RobertWaddell?rel=author" target="_blank">Robert Waddell</a></p>
    </footer>
    <!--- End Footer --->
</div>
