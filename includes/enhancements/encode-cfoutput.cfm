<div class="container container-main">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <h2>Encode CFOUTPUT</h2>
                <p>Up through ColdFusion 11, encoding data inside of <code>&lt;cfoutput&gt;</code> tags required use of a function like <code>EncodeForHTML()</code>.</p>
                <p>In CF2016, you can simply output an entire block with a specific encode type. This is made handy by a new <code>&lt;cfoutput&gt;</code> attribute called "encodefor". The allowed values are "html", "htmlattribute", "javascript", "css", "xml", "xmlattribute", "url", "xpath", "ldap", and "dn" which are equivalent to wrapping each variable output in <code>EncodeForHTML()</code>, <code>EncodeForHTMLAttribute()</code>, etc.</p>
                <p class="lead">This is a great convenience when outputing large data sets which previously resulted in repetitive instances of <code>EncodeForHTML()</code> and the like in the code.</p>
            </div>

            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#syntax" aria-controls="syntax" role="tab" data-toggle="tab">Syntax</a></li>
                    <li role="presentation"><a href="#cf11" aria-controls="cf11" role="tab" data-toggle="tab">CF11 Example</a></li>
                    <li role="presentation"><a href="#cf2016" aria-controls="cf2016" role="tab" data-toggle="tab">CF2016 Example</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="syntax">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>&lt;cfoutput encodefor=&quot;html&quot;&gt;</code><br>
                                <code>&lt;cfoutput encodefor=&quot;htmlattribute&quot;&gt;</code><br>
                                <code>&lt;cfoutput encodefor=&quot;javascript&quot;&gt;</code><br>
                                <code>&lt;cfoutput encodefor=&quot;css&quot;&gt;</code><br>
                                <code>&lt;cfoutput encodefor=&quot;xml&quot;&gt;</code><br>
                                <code>&lt;cfoutput encodefor=&quot;xmlattribute&quot;&gt;</code><br>
                                <code>&lt;cfoutput encodefor=&quot;url&quot;&gt;</code><br>
                                <code>&lt;cfoutput encodefor=&quot;xpath&quot;&gt;</code><br>
                                <code>&lt;cfoutput encodefor=&quot;ldap&quot;&gt;</code><br>
                                <code>&lt;cfoutput encodefor=&quot;dn&quot;&gt;</code><br>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="cf11">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>&lt;cfoutput&gt;</code><br>
                                <code>#EncodeForHtml(FORM.first_name)#&lt;br&gt;</code><br>
                                <code>#EncodeForHtml(FORM.last_name)#&lt;br&gt;</code><br>
                                <code>#EncodeForHtml(FORM.address)#&lt;br&gt;</code><br>
                                <code>#EncodeForHtml(FORM.address2)#&lt;br&gt;</code><br>
                                <code>#EncodeForHtml(FORM.city)#&lt;br&gt;</code><br>
                                <code>#EncodeForHtml(FORM.state)#&lt;br&gt;</code><br>
                                <code>#EncodeForHtml(FORM.zip)#&lt;br&gt;</code><br>
                                <code>#EncodeForHtml(FORM.phone)#&lt;br&gt;</code><br>
                                <code>#EncodeForHtml(FORM.email)#&lt;br&gt;</code><br>
                                <code>&lt;/cfoutput&gt;</code><br>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="cf2016">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>&lt;cfoutput encodefor=&quot;html&quot;&gt;</code><br>
                                <code>&nbsp;&nbsp;#FORM.first_name#&lt;br&gt;</code><br>
                                <code>&nbsp;&nbsp;#FORM.last_name#&lt;br&gt;</code><br>
                                <code>&nbsp;&nbsp;#FORM.address#&lt;br&gt;</code><br>
                                <code>&nbsp;&nbsp;#FORM.address2#&lt;br&gt;</code><br>
                                <code>&nbsp;&nbsp;#FORM.city#&lt;br&gt;</code><br>
                                <code>&nbsp;&nbsp;#FORM.state#&lt;br&gt;</code><br>
                                <code>&nbsp;&nbsp;#FORM.zip#&lt;br&gt;</code><br>
                                <code>&nbsp;&nbsp;#FORM.phone#&lt;br&gt;</code><br>
                                <code>&nbsp;&nbsp;#FORM.email#&lt;br&gt;</code><br>
                                <code>&lt;/cfoutput&gt;</code><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <!--- Begin Footer --->
    <footer>
        <p class="pull-right"><a href="javascript:;" class="toTop">Back to top</a></p>
        <p>&copy; 2016 <a href="https://plus.google.com/u/0/+RobertWaddell?rel=author" target="_blank">Robert Waddell</a></p>
    </footer>
    <!--- End Footer --->
</div>
