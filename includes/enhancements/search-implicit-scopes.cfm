<div class="container container-main">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <h2>Search Implicit Scopes</h2>
                <p>Up through ColdFusion 11, scopes were searched implicitly by default. When a variable was not found in function, local, or variables scopes a search would commence in other scopes such as: CGI, CFFile, URL, Form, Cookie, Client.</p>
                <p>In CF2016, you can optionally disable this implicit search and force all variable scopes to be explicitly set.</p>
                <p class="lead">Performance gains up to <strong>5x</strong> can be achieved.</p>
            </div>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#syntax" aria-controls="syntax" role="tab" data-toggle="tab">Syntax</a></li>
                    <li role="presentation"><a href="#cf11" aria-controls="cf11" role="tab" data-toggle="tab">CF11 Example</a></li>
                    <li role="presentation"><a href="#cf2016" aria-controls="cf2016" role="tab" data-toggle="tab">CF2016 Example</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="syntax">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>this.searchImplicitScopes=true;</code>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="cf11">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>// index.cfm?script_name=test;</code><br><br>
                                <code>writeOutput(script_name)</code><br>
                                <code>// outputs "/<i>path</i>/index.cfm" instead of "test" because script_name exists in CGI scope which is checked before URL scope</code><br><br>
                                <code>writeOutput(CGI.script_name)</code><br>
                                <code>// outputs "/<i>path</i>/index.cfm" because we specify CGI scope</code><br><br>
                                <code>writeOutput(URL.script_name)</code><br>
                                <code>// outputs "test" because we specify URL scope</code>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="cf2016">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>// index.cfm?script_name=test;</code><br><br>
                                <code>writeOutput(script_name)</code><br>
                                <code>// throws undefined error since we aren't searching scopes implicitly</code><br><br>
                                <code>writeOutput(CGI.script_name)</code><br>
                                <code>// outputs "/<i>path</i>/index.cfm" because we specify CGI scope</code><br><br>
                                <code>writeOutput(URL.script_name)</code><br>
                                <code>// outputs "test" because we specify URL scope</code>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <!--- Begin Footer --->
    <footer>
        <p class="pull-right"><a href="javascript:;" class="toTop">Back to top</a></p>
        <p>&copy; 2016 <a href="https://plus.google.com/u/0/+RobertWaddell?rel=author" target="_blank">Robert Waddell</a></p>
    </footer>
    <!--- End Footer --->
</div>
