<div class="container container-main">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <h2>Pass Array By Reference</h2>
                <p>Up through ColdFusion 11, arrays were passed to functions by value. If you modify the passed array, the original array remains untouched. You are forced to return the modified array from the function.</p>
                <p>In CF2016, you can optionally pass arrays to functions by reference. If you modify the passed array, the original array <strong>does</strong> get modified. In this case, there is no need to return a value from the function since the origial array is modified by reference.</p>
                <p>This is controlled by an application setting in application.cfm/application.cfc. This application wide setting must be applied carefully since all arrays passed to functions will be impacted.</p>
                <p class="lead">Gains of <strong>2500%</strong> were seen in Adobe's testing (gains depend on the complexity of the data structure).</p>
            </div>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#syntax" aria-controls="syntax" role="tab" data-toggle="tab">Syntax</a></li>
                    <li role="presentation"><a href="#cf11" aria-controls="cf11" role="tab" data-toggle="tab">CF11 Example</a></li>
                    <li role="presentation"><a href="#cf2016" aria-controls="cf2016" role="tab" data-toggle="tab">CF2016 Example</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="syntax">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>this.passArraybyReference=true;</code>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="cf11">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>arrPowerRangers = [{"zach":"mastodon"},{"jason":"tyrannosaurus"}];</code><br>
                                <code>addValue = function(array arrIn, any valueIn){</code><br>
                                    <code>&nbsp;&nbsp;ArrayAppend(arrIn,valueIn)</code><br>
                                <code>};</code><br><br>
                                <code>// arrPowerRangers is [{"zach":"mastodon"},{"jason":"tyrannosaurus"}]</code><br>
                                <code>addValue(arrPowerRangers,{"tommy":"dragon"});</code><br>
                                <code>// arrPowerRangers still [{"zach":"mastodon"},{"jason":"tyrannosaurus"}]</code><br>
                                <code>// would have to build and return a new array from the function to have an array length of 3</code><br><br>
                                <a class="btn btn-primary" href="<cfoutput>#application.baseUrl#</cfoutput>/before/pass-array-by-value.cfm" target="_blank">Try It</a>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="cf2016">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>this.passArraybyReference=true; // in application.cfc</code><br><br>
                                <code>arrPowerRangers = [{"zach":"mastodon"},{"jason":"tyrannosaurus"}];</code><br>
                                <code>addValue = function(array arrIn, any valueIn){</code><br>
                                    <code>&nbsp;&nbsp;ArrayAppend(arrIn,valueIn)</code><br>
                                <code>};</code><br><br>
                                <code>// arrPowerRangers is [{"zach":"mastodon"},{"jason":"tyrannosaurus"}]</code><br>
                                <code>addValue(arrPowerRangers,{"tommy":"dragon"});</code><br>
                                <code>// arrPowerRangers now [{"zach":"mastodon"},{"jason":"tyrannosaurus"},{"tommy":"dragon"}]</code><br>
                                <code>// Actual array passed in gets modified</code><br><br>
                                <a class="btn btn-primary" href="<cfoutput>#application.baseUrl#</cfoutput>/after/pass-array-by-reference.cfm" target="_blank">Try It</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <!--- Begin Footer --->
    <footer>
        <p class="pull-right"><a href="javascript:;" class="toTop">Back to top</a></p>
        <p>&copy; 2016 <a href="https://plus.google.com/u/0/+RobertWaddell?rel=author" target="_blank">Robert Waddell</a></p>
    </footer>
    <!--- End Footer --->
</div>
