<div class="container container-main">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <h2>PDF Enhancements</h2>
                <p>CF2016 builds on ColdFusion's already robust PDF capabilities.</p>
                <blockquote style="font-size:1em;">
                    <h4>PDF Archival</h4>
                    <p>ColdFusion (2016 release) now supports PDF/A-2b standard for archiving a PDF. PDFs are archived to ensure that the PDF becomes a self contained document that can be opened with any future version of Acrobat Reader, without any issues.</p>
                    <h4>PDF Redaction</h4>
                    <p>The CFPDF tag now has the functionality to redact a PDF. Redaction is used to secure sensitive information in a PDF from being visible to non authorized users. Given a set of coordinates for a paricular PDF, the content within those co-ordinates will be redacted making the content completely invisible in the resulting PDF file.</p>
                    <h4>PDF Sanitization</h4>
                    <p>The CFPDF tag also supports a new action called "sanitize". The process of sanitizing a PDF will make sure that sensitive organization specific information embedded in the metadata or JavaScript or any other sensitive portions of the PDF is not passed on to the audience. Once a PDF is sanitized, the PDF can be safely shared outside of the organization or even as a public facing document without having to worry about sensitive information being shared via the PDF.</p>
                    <h4>Other PDF enhancements</h4>
                    <p>You can now programatically attach and extract attachments to a PDF file. The comments in a PDF file can be exported on to a XFDF file or vice versa. The meta data on the PDF file can also be exported to a XMP file or vice versa thus ensuring a standard meta data on all the PDFs produced by an organization. ColdFusion (2016 release) also allows you to apply stamps on to a PDF file. You can choose from the predefined set or create your own custom stamp as well.</p>
                    <footer>Rakshith Naresh at <cite title="Adobe Developer Center">Adobe Developer Center</cite></footer>
                </blockquote>
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <!--- Begin Footer --->
    <footer>
        <p class="pull-right"><a href="javascript:;" class="toTop">Back to top</a></p>
        <p>&copy; 2016 <a href="https://plus.google.com/u/0/+RobertWaddell?rel=author" target="_blank">Robert Waddell</a></p>
    </footer>
    <!--- End Footer --->
</div>
