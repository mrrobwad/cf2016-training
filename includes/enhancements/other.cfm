<div class="container container-main">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <h2>Other Enhancements</h2>
                <p>Lots has changed and it can't all be covered in detail, so check out the below for more information on other changes/enhancements to CF2016.</p>
            </div>

            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <!---
                    <li role="presentation" class="active"><a href="#dbvarname" aria-controls="dbvarname" role="tab" data-toggle="tab">dbvarname</a></li>
                    --->
                    <li role="presentation" class="active"><a href="#ntlm" aria-controls="ntlm" role="tab" data-toggle="tab">NTLM Auth</a></li>
                    <li role="presentation"><a href="#apimanager" aria-controls="apimanager" role="tab" data-toggle="tab">API Manager</a></li>
                    <li role="presentation"><a href="#configuration" aria-controls="configuration" role="tab" data-toggle="tab">Installation/Configuration</a></li>
                    <li role="presentation"><a href="#swagger" aria-controls="swagger" role="tab" data-toggle="tab">Swagger</a></li>
                    <li role="presentation"><a href="#changedtags" aria-controls="changedtags" role="tab" data-toggle="tab">New/Changed Tags</a></li>
                    <li role="presentation"><a href="#deprecated" aria-controls="deprecated" role="tab" data-toggle="tab">Deprecated</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <!---
                    <div role="tabpanel" class="tab-pane active" id="dbvarname">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <blockquote style="font-size:1em;">
                                    <p>dbvarname is an attribute for cfprocparam to specify named parameters while calling a stored procedure. For details refer <a href="https://wikidocs.adobe.com/wiki/display/coldfusionen/cfprocparam" target="_blank">https://wikidocs.adobe.com/wiki/display/coldfusionen/cfprocparam</a></p>
                                    <p>Versions prior to ColdFusion5 had support for dbvarname. This was deprecated in ColdFusion MX, brought back in 7.0.1 and deprecated again in 7.0.2. The reason was the lack of proper support from multiple jdbc drivers. Since then, this was a major ask among ColdFusion users.</p>
                                    <p>ColdFusion 11 brought this attribute back from the dead. It was disabled by default with a flag to enable it. ColdFusion 11 update 3 enabled it by default. Databases need a variable prefix for named parameters. (eg:- ":" for Oracle and "@" for SQLServer). If the database is SQLServer, it should be dbvarname = "@param1". This will break applications which were using this attribute without the prefix because this attribute was being completely ignored in prior versions. So to give some time for customers who don't want to change their code, in update 4 we introduced a flag to disable dbvarname.</p>
                                    <p>-Dcoldfusion.ignoredbvarname=true jvm flag can be used to disable dbvarname attribute. This flag is only for CF11 and not for later versions.</p>
                                    <footer>Himavanth Rachamsetty at <cite title="Adobe ColdFusion Blog">Adobe ColdFusion Blog</cite></footer>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                    --->
                    <div role="tabpanel" class="tab-pane active" id="ntlm">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <blockquote style="font-size:1em;">
                                    <p>ColdFusion (2016 release) now supports Microsoft's NTLM authentication on CFSHAREPOINT, CFOBJECT and CFINVOKE tags. With this, sharepoint intergration feature set of ColdFusion can now be used with NTLM authentication, just as web services authentication enabled through NTLM authentication.</p>
                                    <footer>Rakshith Naresh at <cite title="Adobe Developer Center">Adobe Developer Center</cite></footer>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="apimanager">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <p>The API Manager is a completely new tool set for ColdFusion 2016.</p>
                                <p>There is a lot to this offering. Rather than exploring it here, check out the link below to start your own exploration.</p>
                                <p><a href="https://helpx.adobe.com/coldfusion/api-manager/features-summary.html" target="_blank">API Manager Features Summary</a></p>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="configuration">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <p>Numerous enhancements were made to installation and configuration of ColdFusion 2016 including:</p>
                                <h4>Removal of Akamai Download Manager</h4>
                                <p>Downloading CF no longer requires the use of Akamai download manager.</p>
                                <h4>API Manager</h4>
                                <p>The CF2016 installation file is now very large because it includes API Manager, even if you don't want it. The installer asks if/how you want to install API Manager (within JVM or as standlone service).</p>
                                <h4>Connector and wsconfig Tool</h4>
                                <p>No longer need to reconfigure connectors after an update. If using multiple instances, the tool now lets you pick which instance to connect to rather than having to start the tool from that instance's bin folder. If you open multiple instances of the tool, you now get a warning that you already have it open. Offers an "all" and an "all -individually" option for create a new connector for each site rather than all together. The tool UI now offers access to connection_pool_timeout, max_reuse_connection, and connection_pool_size tuning arguments (previously only first two).</p>
                                <p>The connector now defaults to timeout of 60 seconds (was 0/infinite). Default settings for IIS are now connection_pool_size=500 and max_reuse_connection=250. CF2016 now includes maxthreads="500" and connectionTimeout="60000" in server.xml by default.</p>
                                <p>"Advanced Settings" now also offers "skip IIS custom errors" option which is disabled by default. Useful for some scenarios (i.e. needed to add manually to isapi_redirect.properties file in CF10/11)</p>
                                <h4>CFAdmin</h4>
                                <p>CFAdmin "enable whitespace management" option now suppresses whitespace at compile time as well as runtime.</p>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="swagger">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <blockquote style="font-size:1em;">
                                    <p>Additional set of meta data have been introduced for REST services so that ColdFusion can generate a swagger document for the REST service defined in the document. ColdFusion (2016 release) will auto generate such a REST description file.</p>
                                    <footer>Rakshith Naresh at <cite title="Adobe Developer Center">Adobe Developer Center</cite></footer>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="changedtags">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h4>Links to details</h4>
                                <p>
                                    <a href="http://helpx.adobe.com/coldfusion/2016/other-enhancements.html" target="_blank">http://helpx.adobe.com/coldfusion/2016/other-enhancements.html</a>
                                    <br>
                                    <a href="http://helpx.adobe.com/coldfusion/2016/language-enhancements.html" target="_blank">http://helpx.adobe.com/coldfusion/2016/language-enhancements.html</a>
                                    <br>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="deprecated">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h4>Links to details</h4>
                                <p>
                                    <a href="http://helpx.adobe.com/coldfusion/deprecated-features.html" target="_blank">http://helpx.adobe.com/coldfusion/deprecated-features.html</a>
                                    <br>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="libraries">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <blockquote style="font-size:1em;">
                                    <h4>Updated Libraries</h4>
                                    <img src="<cfoutput>#application.baseUrl#/assets/images/libraries.png</cfoutput>" class="img-responsive">
                                    <footer>Charlie Arehart</footer>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <!--- Begin Footer --->
    <footer>
        <p class="pull-right"><a href="javascript:;" class="toTop">Back to top</a></p>
        <p>&copy; 2016 <a href="https://plus.google.com/u/0/+RobertWaddell?rel=author" target="_blank">Robert Waddell</a></p>
    </footer>
    <!--- End Footer --->
</div>
