<div class="container container-main">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <h2>Unsynchronized Arrays</h2>
                <p>Up through ColdFusion 11, arrays were thread safe by default. This was at the cost of synchronizing the array object, which prevents multiple threads from accessing the same array simultaneously. Threads have to wait on each other to finish, causing performance slow downs.</p>
                <p>In CF2016, you can optionally create an unsynchronized array and gain a performance boost. This is useful in situations where you know the array will not be accessed by multiple threads at once, such as when the array is defined in a thread safe user defined function.</p>
                <p class="lead">In summary, a normal array remains thread safe but gains no performance boost while an unsynchronized array is not thread safe but gains a <strong>90% performance boost</strong>.</p>
            </div>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#syntax" aria-controls="syntax" role="tab" data-toggle="tab">Syntax</a></li>
                    <li role="presentation"><a href="#cf11" aria-controls="cf11" role="tab" data-toggle="tab">CF11 Example</a></li>
                    <li role="presentation"><a href="#cf2016" aria-controls="cf2016" role="tab" data-toggle="tab">CF2016 Example</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="syntax">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>arrayNew(numeric dimension, boolean isSynchronized=true);</code>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="cf11">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>arrSynchronized = arrayNew(1);</code>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="cf2016">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <code>arrUnsynchronized = arrayNew(1,false);</code>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <!--- Begin Footer --->
    <footer>
        <p class="pull-right"><a href="javascript:;" class="toTop">Back to top</a></p>
        <p>&copy; 2016 <a href="https://plus.google.com/u/0/+RobertWaddell?rel=author" target="_blank">Robert Waddell</a></p>
    </footer>
    <!--- End Footer --->
</div>
