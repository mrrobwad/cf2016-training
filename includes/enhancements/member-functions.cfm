<div class="container container-main">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <h2>New Member Functions</h2>
                <p>CF2016 adds support for more member functions on various objects.</p>
                <h4>Arrays</h4>
                <p>arrayDeleteNoCase</p>
                <h4>Strings</h4>
                <p>encrypt, paragraphFormat, replaceListNoCase, decodeFromURL, decodeForHTML, encodeForHTML, encodeForHTMLAttribute, encodeForXML, encodeForXMLAttribute, encodeForXPath, encodeForCSS, encodeForJavaScript, encodeForURL, getSafeHTML, isSafeHTML, urlDecode, urlEncodedFormat</p>
                <h4>Date/Time</h4>
                <p>LSDateFormat, setSecond, setMinute, setHour, setDay, setMonth, setYear</p>
                <h4>Queries</h4>
                <p>queryEach, queryKeyExists, queryFilter, queryMap, queryReduce, querySort, valueArray</p>
                <p><strong>Note:</strong> If you were previously using query.sort() (undocumented member function), it has now been replaced by a documented version that accepts different arguments and your old code will break. <a href="https://helpx.adobe.com/coldfusion/cfml-reference/coldfusion-functions/functions-m-r/querysort.html" target="_blank">see here</a></p>
                <h4>Display</h4>
                <p>booleanFormat, yesNoFormat</p>
                <h4>More Info</h4>
                <p><a href="http://helpx.adobe.com/coldfusion/developing-applications/building-blocks-of-coldfusion-applications/using-the-member-functions.html" target="_blank">http://helpx.adobe.com/coldfusion/developing-applications/building-blocks-of-coldfusion-applications/using-the-member-functions.html</a><br></p>
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <!--- Begin Footer --->
    <footer>
        <p class="pull-right"><a href="javascript:;" class="toTop">Back to top</a></p>
        <p>&copy; 2016 <a href="https://plus.google.com/u/0/+RobertWaddell?rel=author" target="_blank">Robert Waddell</a></p>
    </footer>
    <!--- End Footer --->
</div>
