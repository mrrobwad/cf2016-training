<div class="container container-main">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <h2>External Session Storage</h2>
                <p>Up through ColdFusion 11, session storage was exclusively handled in memory.</p>
                <p>In CF2016, you can utilize external session storage.</p>
                <p class="lead">External session storage is critical for clustered applications that need to share session data across nodes.</p>
                <p>External session storage is currently only available for use with a Redis server. External session storage also only works for CF sessions, not J2EE sessions.</p>
                <p>Changes to the session are persisted to external storage on request end and then immediately available to all nodes.</p>
                <h4>Setup</h4>
                <p>It couldn't be much easier to setup external/Redis session management in ColdFusion. Simply go into CFAdmin and enter your Redis server and credentials and restart your application. ColdFusion does not setup the Redis server for you, but simply allows you to point to an existing Redis server.</p>
                <img src="<cfoutput>#application.baseUrl#/assets/images/redis-setup.png</cfoutput>" class="img-responsive">
                <h4>Regarding Clusters</h4>
                <p>If you are using ColdFusion cluster management / load balancing, you'll need to disable sticky sessions at the load balancer and clear the Sticky Sessions and Session Replication checkboxes in the ColdFusion Cluster Manager.</p>
                <h4>More Info</h4>
                <p><a href="https://helpx.adobe.com/coldfusion/2016/external-session-storage.html" target="_blank">https://helpx.adobe.com/coldfusion/2016/external-session-storage.html</a><br></p>
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <!--- Begin Footer --->
    <footer>
        <p class="pull-right"><a href="javascript:;" class="toTop">Back to top</a></p>
        <p>&copy; 2016 <a href="https://plus.google.com/u/0/+RobertWaddell?rel=author" target="_blank">Robert Waddell</a></p>
    </footer>
    <!--- End Footer --->
</div>
