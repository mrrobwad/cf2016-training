<div class="container container-main">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <h2>Security Analyzer</h2>
                <p>CF2016 adds an entirely new feature in the Security Analyzer. This tool works only with Enterprise edition and must be run from CFBuilder (though there are standalone hacks out there). Security analyzer scans your ColdFusion codebase (surprisingly quickly) to identify security vulnerabilities. The results include line level vulnerabilties organized by severity. Suggested fixes are also included. Further, a nicely formatted report is generated, suitable for distribution.</p>
                <p>Unfortunately, there is currently no way to whitelist false positives or persist dismissed vulnerabilities. Even if your project falls victim to thousands of false positives, the security scanner is still useful for ad-hoc scans on specific files.</p>
                <h4>Setup</h4>
                <p>It is very simple to run the Security Analyzer if you have CF2016 Enterprise and CFBuilder. Simply ensure you have your server setup in CFBuilder with RDS enabled. Then right click a file or directory (or entire project) and click "Run Security Analyzer" and you're done! Just wait for the report to finish and start fixing vulnerabilities!</p>
                <img src="<cfoutput>#application.baseUrl#/assets/images/security-analyzer-context-menu.png</cfoutput>" class="img-responsive">
                <br><br>
                <img src="<cfoutput>#application.baseUrl#/assets/images/security-analyzer-pane.jpg</cfoutput>" class="img-responsive">
                <br><br>
                <img src="<cfoutput>#application.baseUrl#/assets/images/sample-security-analyzer-report.png</cfoutput>" class="img-responsive">
                <h4>More Info</h4>
                <p><a href="http://www.adobe.com/devnet/coldfusion/articles/security-analyzer.html" target="_blank">http://www.adobe.com/devnet/coldfusion/articles/security-analyzer.html</a><br></p>
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <!--- Begin Footer --->
    <footer>
        <p class="pull-right"><a href="javascript:;" class="toTop">Back to top</a></p>
        <p>&copy; 2016 <a href="https://plus.google.com/u/0/+RobertWaddell?rel=author" target="_blank">Robert Waddell</a></p>
    </footer>
    <!--- End Footer --->
</div>
