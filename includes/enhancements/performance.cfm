<div class="container container-main">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <h2>Built-in Performance</h2>
                <p>Simply upgrading to CF2016 will net some performance gains.</p>
                <p>When testing the performance of the BlogCFC sample application in CF11 vs CF2016, the application was 30% faster in CF2016.</p>
                <ul class="list-group">
                    <li class="list-group-item">
                        <span class="badge">822%</span>
                        Cached Queries
                    </li>
                    <li class="list-group-item">
                        <span class="badge">65%</span>
                        File &amp; List Functions
                    </li>
                    <li class="list-group-item">
                        <span class="badge">22%</span>
                        cfloop tag
                    </li>
                    <li class="list-group-item">
                        <span class="badge">5%</span>
                        Argument Validation
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <hr class="featurette-divider">

    <!--- Begin Footer --->
    <footer>
        <p class="pull-right"><a href="javascript:;" class="toTop">Back to top</a></p>
        <p>&copy; 2016 <a href="https://plus.google.com/u/0/+RobertWaddell?rel=author" target="_blank">Robert Waddell</a></p>
    </footer>
    <!--- End Footer --->
</div>
