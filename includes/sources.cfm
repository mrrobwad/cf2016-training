<div class="container container-main">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <h2>Sources/References</h2>
                <p>This content was compiled largely thanks to the following sources/references.</p>
                <ul>
                    <li><a href="<cfoutput>#application.baseUrl#</cfoutput>/assets/pdf/Himavanth_Final.pdf" target="_blank">Himavanth Rachamsetty CFSummit Presentation</a></li>
                    <li><a href="<cfoutput>#application.baseUrl#</cfoutput>/assets/pdf/Hiddengems_CF2016_CFSummit.pdf" target="_blank">Charlie Arehart CFSummit Presentation</a></li>
                    <li><a href="<cfoutput>#application.baseUrl#</cfoutput>/assets/pdf/WhereisColdFusionHeaded.pdf" target="_blank">Raksith Naresh CFSummit Presentation</a></li>
                    <li><a href="<cfoutput>#application.baseUrl#</cfoutput>/assets/pdf/Keynote-AdobeColdFusionSummit2016-FINAL.pdf" target="_blank">Tridib Roy Chowdhury CFSummit Presentation</a></li>
                    <li><a href="http://www.adobe.com/devnet/coldfusion/articles/whats-new-cf-2016.html" target="_blank">http://www.adobe.com/devnet/coldfusion/articles/whats-new-cf-2016.html</a></li>
                    <li><a href="http://www.adobe.com/devnet/coldfusion/articles/language-enhancements-cf-2016.html" target="_blank">http://www.adobe.com/devnet/coldfusion/articles/language-enhancements-cf-2016.html</a></li>
                    <li><a href="http://helpx.adobe.com/coldfusion/2016/other-enhancements.html" target="_blank">http://helpx.adobe.com/coldfusion/2016/other-enhancements.html</a></li>
                    <li><a href="http://helpx.adobe.com/coldfusion/deprecated-features.html" target="_blank">http://helpx.adobe.com/coldfusion/deprecated-features.html</a></li>
                </ul>
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <!--- Begin Footer --->
    <footer>
        <p class="pull-right"><a href="javascript:;" class="toTop">Back to top</a></p>
        <p>&copy; 2016 <a href="https://plus.google.com/u/0/+RobertWaddell?rel=author" target="_blank">Robert Waddell</a></p>
    </footer>
    <!--- End Footer --->
</div>
