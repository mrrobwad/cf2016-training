<cfcomponent displayname="cf2016training" output="false">

    <!--- Set up the application. --->
    <cfset THIS.Name="cf2016training">
    <cfset THIS.passArrayByReference = true>
    <cfset THIS.searchImplicitScopes = false>

    <cffunction name="onRequest">
        <cfargument name="targetPage" type="String" required="true">
        <cfprocessingdirective suppresswhitespace="yes">
            <cfset application.baseUrl = 'http://localhost/cf2016-training'>
            <cfset application.rootDirPath = "/cf2016-training">
            <cfset application.testLoopLength = 10000>

            <cfset application.meta = {
                title = "ColdFusion 2016 Training",
                description = "ColdFusion 2016 Training"
            }>
            <cfset application.arrMenu = [
                {
                   "Name": "Intro",
                   "Value": "/"
                },
                {
                   "Name": "Enhancements",
                   "Value": [
                        {
                           "Name": "Built-in Performance",
                           "Value": "/enhancements/performance"
                        },
                        {
                           "Name": "Pass Array by Reference",
                           "Value": "/enhancements/pass-array-by-reference"
                        },
                        {
                           "Name": "Unsynchronized Arrays",
                           "Value": "/enhancements/unsynchronized-arrays"
                        },
                        {
                           "Name": "Search Implicit Scopes",
                           "Value": "/enhancements/search-implicit-scopes"
                        },
                        {
                           "Name": "Ordered Structs",
                           "Value": "/enhancements/ordered-structs"
                        },
                        {
                           "Name": "Struct Serialization",
                           "Value": "/enhancements/struct-serialization"
                        },
                        {
                           "Name": "Safe Navigation",
                           "Value": "/enhancements/safe-navigation"
                        },
                        {
                           "Name": "Encode CFOUTPUT",
                           "Value": "/enhancements/encode-cfoutput"
                        },
                        {
                           "Name": "PDF Enhancements",
                           "Value": "/enhancements/pdf"
                        },
                        {
                           "Name": "Member Functions",
                           "Value": "/enhancements/member-functions"
                        },
                        {
                           "Name": "Redis Sessions",
                           "Value": "/enhancements/redis-sessions"
                        },
                        {
                           "Name": "CLI",
                           "Value": "/enhancements/cli"
                        },
                        {
                           "Name": "Security Analyzer",
                           "Value": "/enhancements/security-analyzer"
                        },
                        {
                           "Name": "Other",
                           "Value": "/enhancements/other"
                        }
                    ]
                },
                {
                   "Name": "Aether Preview",
                   "Value": "/aether"
                },
                {
                   "Name": "Sources/References",
                   "Value": "/sources"
                }
            ]>

            <cfinclude template="#ARGUMENTS.targetPage#">
        </cfprocessingdirective>
    </cffunction>

</cfcomponent>
