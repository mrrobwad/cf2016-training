<cfcomponent displayname="cf2016trainingBEFORE" output="false">

    <!--- Set up the application. --->
    <cfset THIS.Name="cf2016trainingBEFORE">

    <cffunction name="OnApplicationStart" access="public" returntype="boolean" output="false" hint="Fires when the application is first created.">
        <cfreturn true>
    </cffunction>

    <cffunction name="onRequest">
        <cfargument name="targetPage" type="String" required="true">
        <cfprocessingdirective suppresswhitespace="yes">
            <cfset application.testLoopLength = 10000>
            <cfinclude template="#ARGUMENTS.targetPage#">
        </cfprocessingdirective>
    </cffunction>

</cfcomponent>
