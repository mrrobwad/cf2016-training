<cfparam name="URL.page" default="">
<cfparam name="URL.subpage" default="">
<cfset contentIncludePath = "">
<cfif URL.page neq "" AND NOT Find("##",URL.page)>
    <cfset contentIncludePath = "includes/" & URL.page>
    <cfif URL.subpage neq "">
        <cfset contentIncludePath &= "/" & URL.subpage & ".cfm">
    <cfelse>
        <cfset contentIncludePath &= ".cfm">
    </cfif>
</cfif>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<cfoutput>#application.meta.description#</cfoutput>">
    <meta name="author" content="Robert Waddell">
    <link rel="icon" href="<cfoutput>#application.baseUrl#</cfoutput>/assets/favicon.ico">

    <title><cfoutput>#application.meta.title#</cfoutput></title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="<cfoutput>#application.baseUrl#</cfoutput>/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<cfoutput>#application.baseUrl#</cfoutput>/assets/css/carousel.css" rel="stylesheet">
    <link href="<cfoutput>#application.baseUrl#</cfoutput>/assets/css/custom.css" rel="stylesheet">
</head>
<body class="<cfoutput>#URL.page# #URL.subpage#</cfoutput> <cfif contentIncludePath neq "">inner</cfif>">
    <!--- Begin Menu System --->
    <div class="navbar-wrapper">
        <div class="container">
            <nav class="navbar navbar-inverse navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <cfoutput>
                        <a class="navbar-brand" href="#application.baseUrl#">#application.meta.title#</a>
                        </cfoutput>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <cfloop array="#application.arrMenu#" index="currMenuItem">
                                <cfset blnDirectory = false>
                                <cfset blnActive = false>
                                <cfset currMenuItemName = currMenuItem.Name>
                                <cfset currMenuItemValue = currMenuItem.Value>
                                <cfif IsArray(currMenuItemValue)>
                                    <cfset blnDirectory = true>
                                <cfelse>
                                    <cfif currMenuItemValue eq URL.page & "/" & URL.subpage>
                                        <cfset blnActive = true>
                                    </cfif>
                                </cfif>
                                <cfif NOT blnDirectory>
                                    <cfoutput>
                                        <li <cfif blnActive>class="active"</cfif>><a href="#application.baseUrl##currMenuItemValue#">#currMenuItemName#</a></li>
                                    </cfoutput>
                                <cfelse>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><cfoutput>#currMenuItemName#</cfoutput> <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <cfloop array="#currMenuItemValue#" index="currSubMenuItem">
                                                <cfset currSubMenuItemName = currSubMenuItem.Name>
                                                <cfset currSubMenuItemValue = currSubMenuItem.Value>
                                                <cfif currSubMenuItemValue eq URL.page & "/" & URL.subpage>
                                                    <cfset blnActive = true>
                                                </cfif>
                                                <cfoutput>
                                                    <li <cfif blnActive>class="active"</cfif>><a href="#application.baseUrl##currSubMenuItemValue#">#currSubMenuItemName#</a></li>
                                                </cfoutput>
                                            </cfloop>
                                        </ul>
                                    </li>
                                </cfif>
                            </cfloop>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <!--- End Menu System --->

    <!--- Begin Content --->
    <cfswitch expression="#contentIncludePath#">
        <cfcase value="">
            <cfinclude template="#application.rootDirPath#/includes/home.cfm">
        </cfcase>
        <cfdefaultcase>
            <cfinclude template="#application.rootDirPath#/#contentIncludePath#">
        </cfdefaultcase>
    </cfswitch>
    <!--- End Content --->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.bundle.min.js"></script>
    <script src="<cfoutput>#application.baseUrl#</cfoutput>/assets/js/app.js"></script>
</body>
</html>
